CC=i686-elf-gcc

NASM=nasm
NASM_FLAGS=-felf32

KINCLUDE_DIR=kinclude
INCLUDE_DIR=include

CFLAGS=-nostdlib -fno-use-linker-plugin -std=gnu99 -ffreestanding -O2 -Wall -Wextra -I$(KINCLUDE_DIR) -I$(INCLUDE_DIR)

BIN_NAME=cbos.bin
ISO_OUTPUT=cbos.iso
