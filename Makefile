include build.mk

all: build_cbos generate_iso


build_cbos:
		$(MAKE) -C boot all
		$(MAKE) -C cbos all
		mkdir -p iso/boot/grub
		cp cbos/cbos.bin iso/boot/
.PHONY generate_iso:
		$(shell ./gen_iso.sh)
		
		
.PHONY run:
	qemu-system-i386 -cdrom $(ISO_OUTPUT)





.PHONY clean:
	@$(MAKE) -C boot clean
	@$(MAKE) -C cbos clean
	rm -rf $(BIN_NAME) *.iso
	rm -rf iso/