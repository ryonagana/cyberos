

/*
	first implementatios of  stdlibs
*/

#ifndef  _STDIO_H_
#define _STDIO_H_

#define FOPEN_MAX (20)
#define FILENAME_MAX (1024) 


#ifndef L_tmpnam 
#define L_tmpnam 1024
#endif



#ifndef TMP_MAX
#define TMP_MAX 308915776   /* same value in <limits.h> */
#endif


#ifndef SEEK_SET
#define SEEK_SET 0
#endif

#ifndef SEEK_CUR
#define SEEK_CUR 1
#endif

#ifndef SEEK_END
#define SEEK_END 2
#endif 


/* decls */
int getchar();
void printf(const char _t, ...);
void puts(const char *)
#endif