inline unsigned char _inportb(unsigned int port);
inline void _outportb(unsigned int port, unsigned char data);
#define outb(p,d) _outportb( (unsigned int) p, (unsigned char) d)
#define inb(p) _inportb( (unsigned int) p)
