#include <stdint.h>
#include <kprint.h>

void kprintf(const char *msg){
	return;

}

static inline unsigned int  vga_entry_color(enum VGA_COLOR foreground, enum VGA_COLOR background)
{
	return foreground | background << 4;
}

static inline uint16_t vga_entry(unsigned char c, uint8_t color)
{
	return (uint16_t) c | (uint16_t) color << 8;
}

static unsigned int vga_strlen(const char* str) 
{
	unsigned int len = 0;
	while (str[len])
		len++;
	return len;
}

static const int VGA_WIDTH = 80;
static const int VGA_HEIGHT = 25;

unsigned int vga_row;
unsigned int vga_column;
uint8_t vga_color;
uint16_t* vga_buffer;

void init_video(void)
{
		vga_row = 0;
		vga_column = 0;
		vga_color = vga_entry_color(VGA_COLOR_MAGENTA, VGA_COLOR_WHITE);
		
		vga_buffer = (unsigned char*) 0xB8000;
		
		int y,x;
		
		for(y = 0; y < VGA_HEIGHT; y++){
			for(x = 0; x < VGA_WIDTH; x++){
				const unsigned int index = y * VGA_WIDTH * x;
				vga_buffer[index] = vga_entry(' ', vga_color);
			}
		}
		
}
		
		
		
void vga_setcolor(enum VGA_COLOR color)
{
	vga_color = (int) color;
}

void vga_putentry(char c, uint8_t color, unsigned int x, unsigned int y)
{
	const unsigned int index = y * VGA_WIDTH + x;
	vga_buffer[index] = vga_entry(c, color);
}

void vga_putchar(char c) 
{
	vga_putentry(c, vga_color, vga_column, vga_row);
	if (++vga_column == VGA_WIDTH) {
		vga_column = 0;
		if (++vga_row == VGA_HEIGHT)
			vga_row = 0;
	}
}


void vga_write(const char *data, unsigned int size)
{
	int i;
	for(i = 0; i < size;i++){
		vga_putchar(data[i]);
	}
}



void kprintfc(int color, const char *string)
{
	vga_write(string, vga_strlen(string));
}
