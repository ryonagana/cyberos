#include <knmi.h>
#include <io.h>


void NMI_enable()
{
	outb(0x70, inb(0x70) & 0x7F);
	return;
}

void NMI_disable(){
	outb(0x70, inb(0x70) | 0x80);
	return;
}

void StopInterrupt()
{
	asm("cli");
}